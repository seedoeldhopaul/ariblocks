/* 
 * Modify this to change the output of the status command.
 * You can use the pango markup format.
 */

/* Right block */
static const struct ari_block blocks_right[] = {
    /* Icon   , Command                                              , Interval , Signal */
    {"📆 "    , "date +'%a %b %D %R'"                                , 30       , 0}        ,
    {"Free: " , "free -h | awk '/^Mem/ { print $4 }' | sed 's/i//g'" , 60       , 0}        ,
    {"🐕 "    , "echo Be kind to others."                            , 0        , 0}        ,
};

/* Left block */
static const struct ari_block blocks_left[] = {
    /* Icon , Command                   , Interval , Signal */
    {""     , "echo '<i>ariblocks</i>'" , 0        , 0}        ,
};

static char delim[] = " | ";
static unsigned char delimlen = 5;
static double bg_color[3] = {0.0, 0.0, 0.0};
static double font_color[3] = {1.0, 1.0, 1.0};
