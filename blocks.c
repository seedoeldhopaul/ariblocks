#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <signal.h>

#include "window.h"

#define LENGTH(x)   (sizeof x / sizeof x[0])
#define NUM_BLOCKS_RIGHT    (LENGTH(blocks_right))
#define NUM_BLOCKS_LEFT     (LENGTH(blocks_left))
#define CMDLENGTH   1250
#define MIN(a, b)   ((a < b) ? (a) : (b))
#define STATUSLENGTH_RIGHT    (NUM_BLOCKS_RIGHT * CMDLENGTH + 1)
#define STATUSLENGTH_LEFT    (NUM_BLOCKS_LEFT * CMDLENGTH + 1)

struct ari_block {
    char *icon;
    char *command;
    unsigned int interval;
    unsigned int signal;
};

#include "blocks.h"

static struct window *root = NULL;
static char statusbar_right[NUM_BLOCKS_RIGHT][CMDLENGTH] = {0};
static char statusbar_left[NUM_BLOCKS_LEFT][CMDLENGTH] = {0};
static char statusstr_right[2][STATUSLENGTH_RIGHT];
static char statusstr_left[2][STATUSLENGTH_LEFT];
static bool status_continue = true;
static void (*writestatus)(void) = NULL;

static void
getcmd(const struct ari_block *block, char *output)
{
    strcpy(output, block->icon);

    FILE *cmdf = popen(block->command, "r");
    if (!cmdf)
        return;
    int i = strlen(block->icon);
    fgets(output + i, CMDLENGTH - i - delimlen, cmdf);
    pclose(cmdf);

    i = strlen(output);
    if (i == 0)
        return;

    i = output[i - 1] == '\n' ? i - 1 : i;

    strncpy(output + i, delim, delimlen);
}

static void
getcmds(int time)
{
    const struct ari_block *current;

    for (unsigned int i = 0; i < NUM_BLOCKS_RIGHT; i++) {
        current = blocks_right + i;
        if ((current->interval != 0 && time % current->interval == 0) || time == -1)
            getcmd(current, statusbar_right[i]);
    }

    for (unsigned int i = 0; i < NUM_BLOCKS_LEFT; i++) {
        current = blocks_left + i;
        if ((current->interval != 0 && time % current->interval == 0) || time == -1)
            getcmd(current, statusbar_left[i]);
    }
}

static void
getsigcmds(unsigned int signum)
{
    const struct ari_block *current;

    for (unsigned int i = 0; i < NUM_BLOCKS_RIGHT; i++) {
        current = blocks_right + i;
        if (current->signal == signum)
            getcmd(current, statusbar_right[i]);
    }

    for (unsigned int i = 0; i < NUM_BLOCKS_LEFT; i++) {
        current = blocks_left + i;
        if (current->signal == signum)
            getcmd(current, statusbar_left[i]);
    }
}

static void
sighandler(int signum)
{
    getsigcmds(signum - SIGRTMIN);
    writestatus();
}

static void
termhandler(int arg)
{
    status_continue = false;
    window_destroy(root);
}

static void
setupsignals(void)
{
    for (unsigned int i = 0; i < NUM_BLOCKS_RIGHT; i++) {
        if (blocks_right[i].signal > 0)
            signal(SIGRTMIN+blocks_right[i].signal, sighandler);
    }

    for (unsigned int i = 0; i < NUM_BLOCKS_LEFT; i++) {
        if (blocks_left[i].signal > 0)
            signal(SIGRTMIN+blocks_left[i].signal, sighandler);
    }
}

static void
statusloop(void)
{
    setupsignals();
    root = window_create();
    window_setup(root);
    window_set_bg_color(root, bg_color);
    window_set_font_color(root, font_color);
    getcmds(-1);
    for (int i = 0; status_continue; i++) {
        getcmds(i);
        writestatus();
        sleep(1.0);
    }
}

static int
getstatus(char *str_r, char *last_r, char *str_l, char *last_l)
{
    strcpy(last_r, str_r);
    str_r[0] = '\0';
    for (unsigned int i = 0; i < NUM_BLOCKS_RIGHT; i++)
        strcat(str_r, statusbar_right[i]);
    str_r[strlen(str_r) - strlen(delim)] = '\0';

    strcpy(last_l, str_l);
    str_l[0] = '\0';
    for (unsigned int i = 0; i < NUM_BLOCKS_LEFT; i++)
        strcat(str_l, statusbar_left[i]);
    str_l[strlen(str_l) - strlen(delim)] = '\0';

    return strcmp(str_r, last_r) && strcmp(str_l, last_l);
}

static void
pstatusbar()
{
    bool unchanged = getstatus(statusstr_right[0], statusstr_right[1], statusstr_left[0], statusstr_left[1]);
    if (unchanged)
        return;

    window_set_status(root, statusstr_right[0], statusstr_left[0]);
    window_run(root);
}

static void 
pstdout()
{
    bool unchanged = getstatus(statusstr_right[0], statusstr_right[1], statusstr_left[0], statusstr_left[1]);
    if (unchanged)
        return;

    printf("Right: %s\n, Left: %s\n", statusstr_right[0], statusbar_left[0]);
    fflush(stdout);
}

int
main(int argc, char **argv)
{
    writestatus = pstatusbar;
    for (int i = 0; i < argc; i++) {
        if (strcmp("-d", argv[i]) == 0)
            strncpy(delim, argv[++i], delimlen);
        else if (strcmp("-p", argv[i]) == 0)
            writestatus = pstdout;
    }

    delimlen = MIN(delimlen, strlen(delim));
    delim[delimlen++] = '\0';
    signal(SIGTERM, termhandler);
    signal(SIGINT, termhandler);
    statusloop();
}
