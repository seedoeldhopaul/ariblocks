#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>
#include <sys/mman.h>
#include <unistd.h>
#include <wayland-client.h>
#include <cairo.h>
#include <pango/pangocairo.h>
#include "zwlr-layer-shell-unstable-v1.h"
#include "xdg-shell.h"
#include "util.h"

#include "window.h"


struct window {
    /* Globals */
    struct wl_display *display;
    struct wl_registry *registry;
    struct wl_compositor *compositor;
    struct wl_output *output;
    struct zwlr_layer_shell_v1 *layer_shell;
    struct wl_shm *shm;

    struct wl_surface *surface;
    struct zwlr_layer_surface_v1 *layer_surface;
    int width, height;

    const char *statusmsg_right;
    const char *statusmsg_left;
    char *stripped_msg_right;
    char *stripped_msg_left;
    double bg_color[3];
    double font_color[3];
};

static void
output_geometry(void *data, struct wl_output *output,
        int x, int y, int physical_width, int physical_height,
        int subpixel, const char *make, const char *model,
        int transform)
{
    /* Do nothing */
}

static void
output_mode(void *data, struct wl_output *output,
        uint32_t flags, int width, int height, int refresh)
{
    struct window *window = data;
    window->width = width;
    window->height = height * 0.025;
}

static void
output_done(void *data, struct wl_output *output)
{
    /* Do nothing */
}

static void
output_scale(void *data, struct wl_output *output, int factor)
{
    /* Do nothing */
}

static const struct wl_output_listener output_listener = {
    .geometry = output_geometry,
    .mode = output_mode,
    .done = output_done,
    .scale = output_scale,
};

static void
handle_global(void *data, struct wl_registry *registry, uint32_t id,
        const char *interface, uint32_t version)
{

    struct window *window = data;

    if (strcmp(interface, wl_compositor_interface.name) == 0) {
        window->compositor = wl_registry_bind(registry, id, &wl_compositor_interface, version);
    } else if (strcmp(interface, zwlr_layer_shell_v1_interface.name) == 0) {
        window->layer_shell = wl_registry_bind(registry, id, &zwlr_layer_shell_v1_interface, version);
    } else if (strcmp(interface, wl_output_interface.name) == 0) {
        window->output = wl_registry_bind(registry, id, &wl_output_interface, version);
        wl_output_add_listener(window->output, &output_listener, window);
    } else if (strcmp(interface, wl_shm_interface.name) == 0)
        window->shm = wl_registry_bind(registry, id, &wl_shm_interface, version);
}

static void
handle_global_remove(void *data, struct wl_registry *registry, uint32_t id)
{
    /* Do nothing */
}

static const struct wl_registry_listener registry_listener = {
    .global = handle_global,
    .global_remove = handle_global_remove,
};

static void
buffer_release(void *data, struct wl_buffer *buffer)
{
    wl_buffer_destroy(buffer);
}

static struct wl_buffer_listener buffer_listener = {
    .release = buffer_release,
};

static void
draw_pango(cairo_t *cr, int width, int height, const char *markup_text_right, char *stripped_text_right, const char *markup_text_left, char *stripped_text_left)
{
    PangoAttrList *attrlist;
    PangoRectangle rect;
    PangoFontDescription *desc;
    PangoLayout *layout;

    attrlist = pango_attr_list_new();

    // Default font
    desc = pango_font_description_from_string("Noto Sans Bold 8");
    layout = pango_cairo_create_layout(cr);
    pango_layout_set_font_description(layout, desc);

    // Right blocks
    pango_parse_markup(markup_text_right, -1, 0, &attrlist, &stripped_text_right, NULL, NULL);
    pango_layout_set_attributes(layout, attrlist);
    pango_layout_set_text(layout, stripped_text_right, strlen(stripped_text_right));
    
    // The text is right aligned, adjust the (x, y) position
    pango_layout_get_pixel_extents(layout, &rect, NULL);
    cairo_move_to(cr, width - rect.width, height * 0.20);
    pango_cairo_show_layout(cr, layout);

    // Left blocks
    pango_parse_markup(markup_text_left, -1, 0, &attrlist, &stripped_text_left, NULL, NULL);
    pango_layout_set_attributes(layout, attrlist);
    pango_layout_set_text(layout, stripped_text_left, strlen(stripped_text_left));
    
    // Left aligned text, move to the beginning
    cairo_move_to(cr, 0, height * 0.20);
    pango_cairo_show_layout(cr, layout);

    g_object_unref(layout);
    pango_attr_list_unref(attrlist);
}

static void
draw_cairo(uint32_t *data, 
        int width, int height, int stride, 
        const char *text_right, char *stripped_text_right,
        const char *text_left, char *stripped_text_left, 
        const double bgcolor[static 3],
        const double fontcolor[static 3])
{
    union {
        uint32_t *ui;
        unsigned char *uc;
    } mem;
    mem.ui = data;

    cairo_surface_t *surface = cairo_image_surface_create_for_data(mem.uc,
            CAIRO_FORMAT_ARGB32, width, height, stride);
    cairo_t *cr = cairo_create(surface);

    cairo_set_source_rgb(cr, bgcolor[0], bgcolor[1], bgcolor[2]);
    
    cairo_rectangle(cr, 0, 0, width, height);
    cairo_fill(cr);

    cairo_set_source_rgb(cr, fontcolor[0], fontcolor[1], fontcolor[2]);

    draw_pango(cr, width, height, 
            text_right, stripped_text_right,
            text_left, stripped_text_left);

    cairo_surface_flush(surface);

    cairo_destroy(cr);
    cairo_surface_destroy(surface);
}

static struct wl_buffer *
draw_frame(struct window *window)
{
    const int width = window->width;
    const int height = window->height;
    int stride = width * sizeof width;
    int size = stride * height;

    int fd = allocate_shm_file(size);
    if (fd == -1)
        return NULL;

    uint32_t *data = mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if (data == MAP_FAILED) {
        close(fd);
        return NULL;
    }

    struct wl_shm_pool *pool = wl_shm_create_pool(window->shm, fd, size);
    struct wl_buffer *buffer = wl_shm_pool_create_buffer(pool, 0,
            width, height, stride, WL_SHM_FORMAT_XRGB8888);
    wl_shm_pool_destroy(pool);
    close(fd);

    draw_cairo(data, width, height, stride, 
            window->statusmsg_right,
            window->stripped_msg_right,
            window->statusmsg_left,
            window->stripped_msg_left,
            window->bg_color,
            window->font_color);

    munmap(data, size);
    wl_buffer_add_listener(buffer, &buffer_listener, NULL);
    return buffer;
}

static const struct wl_callback_listener frame_listener;

static void
frame_handle_done(void *data, struct wl_callback *callback, uint32_t time)
{
    struct window *window = data;
    wl_callback_destroy(callback);

    wl_surface_damage(window->surface, 0, 0, window->width, window->height);
    struct wl_buffer *buffer = draw_frame(window);
    // Re-register a frame callback
    callback = wl_surface_frame(window->surface);
    wl_callback_add_listener(callback, &frame_listener, window);
    wl_surface_attach(window->surface, buffer, 0, 0);
    wl_surface_commit(window->surface);
}

static const struct wl_callback_listener frame_listener = {
    .done = frame_handle_done,
};

static void
layer_surface_configure(void *data, struct zwlr_layer_surface_v1 *layer_surface,
        uint32_t serial, uint32_t w, uint32_t h)
{
    struct window *window = data;

    window->width = w;
    window->height = h;
    zwlr_layer_surface_v1_ack_configure(layer_surface, serial);

    // Register a frame callback to know when to draw next time
    struct wl_callback *callback = wl_surface_frame(window->surface);
    wl_callback_add_listener(callback, &frame_listener, window);

    struct wl_buffer *buffer = draw_frame(window);
    wl_surface_attach(window->surface, buffer, 0, 0);
    wl_surface_commit(window->surface);
}

static void
layer_surface_closed(void *data, struct zwlr_layer_surface_v1 *layer_surface)
{
    struct window *window = data;
    zwlr_layer_surface_v1_destroy(layer_surface);
    wl_surface_destroy(window->surface);
}

static const struct zwlr_layer_surface_v1_listener layer_surface_listener = {
    .configure = layer_surface_configure,
    .closed = layer_surface_closed,
};

struct window *
window_create(void)
{
    struct window *window = calloc(1, sizeof *window);
    window->statusmsg_right = "";
    window->statusmsg_left = "";

    return window;
}

void
window_set_bg_color(struct window *window, const double color[static 3])
{
    memcpy(window->bg_color, color, sizeof window->bg_color);
}

void
window_set_font_color(struct window *window, const double color[static 3])
{
    memcpy(window->font_color, color, sizeof window->font_color);
}

void
window_setup(struct window *window)
{
    window->display = wl_display_connect(NULL);
    assert(window->display);

    window->stripped_msg_right = calloc(1, 3500);
    window->stripped_msg_left = calloc(1, 3500);
    assert(window->stripped_msg_right);
    assert(window->stripped_msg_left);

    window->registry = wl_display_get_registry(window->display);
    wl_registry_add_listener(window->registry, &registry_listener, window);

    wl_display_dispatch(window->display);
    wl_display_roundtrip(window->display);

    assert(window->compositor);
    assert(window->layer_shell);
    assert(window->output);
    assert(window->shm);

    window->surface = wl_compositor_create_surface(window->compositor);
    assert(window->surface);

    window->layer_surface = zwlr_layer_shell_v1_get_layer_surface(window->layer_shell,
            window->surface, window->output, ZWLR_LAYER_SHELL_V1_LAYER_TOP, "ariblocks");
    assert(window->layer_surface);

    zwlr_layer_surface_v1_set_size(window->layer_surface, window->width, window->height);
    zwlr_layer_surface_v1_set_anchor(window->layer_surface, ZWLR_LAYER_SURFACE_V1_ANCHOR_BOTTOM);// | ZWLR_LAYER_SURFACE_V1_ANCHOR_RIGHT);
    zwlr_layer_surface_v1_set_exclusive_zone(window->layer_surface, window->height);
    zwlr_layer_surface_v1_add_listener(window->layer_surface, &layer_surface_listener, window);
    wl_surface_commit(window->surface);
    wl_display_roundtrip(window->display);
}


bool
window_run(struct window *window)
{
    wl_display_roundtrip(window->display);
    return wl_display_dispatch(window->display) != -1;
}

void
window_set_status(struct window *window,
        const char *statusmsg_right,
        const char *statusmsg_left)
{
    window->statusmsg_right = statusmsg_right;
    window->statusmsg_left = statusmsg_left;
}

void
window_destroy(struct window *window)
{
    wl_display_disconnect(window->display);
    free(window->stripped_msg_right);
    free(window->stripped_msg_left);
    free(window);
}
