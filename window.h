#ifndef _WINDOW_H
#define _WINDOW_H

#include <stdbool.h>

struct window;

struct window *
window_create(void);

void
window_setup(struct window *window);

void
window_set_bg_color(struct window *window, const double color[static 3]);

void
window_set_font_color(struct window *window, const double color[static 3]);

bool 
window_run(struct window *window);

void 
window_set_status(struct window *window,
        const char *statusmsg_right,
        const char *statusmsg_left);

void 
window_destroy(struct window *window);

#endif
