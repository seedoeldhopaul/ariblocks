#ifndef _UTIL_H
#define _UTIL_H

#include <stddef.h>

int
allocate_shm_file(size_t size);

#endif
