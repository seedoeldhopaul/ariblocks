WAYLAND_PROTOCOLS=$(shell pkg-config --variable=pkgdatadir wayland-protocols)
WAYLAND_SCANNER=$(shell pkg-config --variable=wayland_scanner wayland-scanner)
LIBS=\
	 $(shell pkg-config --cflags --libs pangocairo wayland-client) \
	 -lrt

SRC=blocks.c util.c window.c xdg-shell.c zwlr-layer-shell-unstable-v1.c
DEPS=blocks.h util.h window.h xdg-shell.h zwlr-layer-shell-unstable-v1.h

zwlr-layer-shell-unstable-v1.h:
	$(WAYLAND_SCANNER) client-header \
		protocols/wlr-layer-shell-unstable-v1.xml $@

zwlr-layer-shell-unstable-v1.c:
	$(WAYLAND_SCANNER) private-code \
		protocols/wlr-layer-shell-unstable-v1.xml $@

xdg-shell.h:
	$(WAYLAND_SCANNER) client-header \
		$(WAYLAND_PROTOCOLS)/stable/xdg-shell/xdg-shell.xml $@

xdg-shell.c:
	$(WAYLAND_SCANNER) private-code \
		$(WAYLAND_PROTOCOLS)/stable/xdg-shell/xdg-shell.xml $@

blocks.h: blocks.def.h
	cp $< $@ 

ariblocks: $(SRC) $(DEPS)
	$(CC) -Wall -Werror -I. \
		$(LIBS) -o $@ -g\
		$(SRC)

clean:
	rm -f ariblocks zwlr-layer-shell-unstable-v1.h zwlr-layer-shell-unstable-v1.c \
		xdg-shell.c xdg-shell.h

.DEFAULT_GOAL=ariblocks
.PHONY: clean
